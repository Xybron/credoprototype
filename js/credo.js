const asyncLocalStorage = {
    setItem: function (key, value) {
        return Promise.resolve().then(function () {
            localStorage.setItem(key, value);
        });
    },
    getItem: function (key) {
        return Promise.resolve().then(function () {
            return localStorage.getItem(key);
        });
    }
};

if(document.getElementById("splashscreen")){
    new Vue({
        el : '#credo',
        data : {
            splashscreenState :true
        },
        mounted : function(){
            setTimeout(()=>{
                window.location.href = "../pages/authPage.html";
            },1500);
        }
    });
}

if(document.getElementById("authPage")){
    new Vue({
        el : '#loginPage',
        data : {
            username : "",
            password : "",
            loginIsClicked : false
        },
        mounted : function(){
            
        },
        methods: {
            login : function(){
                this.loginIsClicked = true;
                if(this.username == "RakataLabs" && this.password == "rakata"){
                    window.location.href = "../pages/dashboard.html";
                }
                else{
                    alert("Invalid credentials! Ask Ronald for the credentials.")
                    this.loginIsClicked = false;
                }
                
            }
        },
    });
}

if(document.getElementById("dashboard")){
    new Vue({
        el : '#dashboard',
        data : {
            
        },
        mounted : function(){
            
        },
        methods: {
          goToInvestmentProduct : function(id){
            asyncLocalStorage.setItem('inProduct', id).then(succ=>{
                window.location.href = "../pages/investmentProduct.html";
            }).catch(e=>{
                alert("Could not load product!")
            })
          }
        },
    });
}

if(document.getElementById("investmentProductPage")){
    new Vue({
        el : '#investmentProductPage',
        data : {
            id : "",
        },
        mounted : function(){
           asyncLocalStorage.getItem('inProduct').then(result=>{
            this.id = result;
           });
            
        },
        methods: {
        
        },
    });
}